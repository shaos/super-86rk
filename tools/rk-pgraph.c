/*
 rk-pgraph.c - Converting grayscale image 128x50 to RK86 pseudo-graphics by Shaos

 This code is PUBLIC DOMAIN! Use it on your own RISK!!!

 Build: gcc rk-pgraph.c -lpng -ork-pgraph

 http://www.nedopc.org/forum/viewtopic.php?f=93&t=22389&start=45

 24-MAR-2024 - started coding
 31-MAR-2024 - working version ready (ASM)
 02-APR-2024 - released version (ASM and BIN)

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <png.h>

//#define DEBUG

int count[8] = {
 0, // 000
 1, // 001
 1, // 010
 2, // 011
 1, // 100
 2, // 101
 2, // 110
 3  // 111
};

int hist[256],allz[16];
char chars[256][8];

int main(int argc, char **argv)
{
 int i,j,k,a,b,c,e,e1,e2,x,y,z,z1,z2,hh,h=5,m=0;
 FILE *f,*fb;
 if(argc<2)
 {
   printf("\nrk-pgraph filename.png [lines]\n\n");
   return 0;
 }
 if(argc>2) h=atoi(argv[2]);
 a = 256/(3*h+1);
 f = fopen("sgr.bin","rb");
 if(f==NULL){printf("\nNeed sgr.bin\n\n");return -1;}
 for(i=0;i<256;i++)
 {
   hist[i] = 0;
   for(j=0;j<8;j++) chars[i][j] = 0;
 }
 chars[0][0] = 0x20; // space for blank
 for(i=0;i<127;i++)
 {
   z1 = z2 = 0;
   for(j=0;j<8;j++)
   {
     c = (~fgetc(f))&63;
     if(j>=h) continue;
     if(i>=8 && i<=15) continue;
     if(i>=24 && i<=31) continue;
//     printf("%2.2X\n",c);
     z1 += count[c>>3];
     z2 += count[c&7];
   }
   c = (z1<<4)|z2;
   printf("%i) %i %i (%2.2X)\n",i,z1,z2,c);
   hist[c]++;
   if(c)
   {
     k = 0;
     while(chars[c][k])
     {
        if(++k==8){k=7;break;}
     }
     chars[c][k] = i;
   }
 }
 printf("\n");
 fclose(f);
 c = 0;
 for(j=0;j<16;j++)
 {
   if(j==0)
   {
     printf("  | 0 1 2 3 4 5 6 7 8 9 A B C D E F\n");
     printf("-----------------------------------\n");
   }
   z = 0;
   for(i=0;i<16;i++)
   {
     if(i==0) printf("%X |",j);
     if(i!=0&&j!=0) if(hist[c]>m) m=hist[c];
     hh = hist[c++];
     printf(" %X",(hh>15)?15:hh);
     z += hh;
   }
   allz[j] = (z==0)?1:0;
   printf("\n");
 }
 printf("\nmax num: %i\n\n",m);
 c = 0;
 for(j=0;j<16;j++)
 {
   for(i=0;i<16;i++)
   {
     hh = hist[c];
     if(!hh){c++;continue;}
     printf("%2.2X:",c);
     for(k=0;k<8;k++)
     {
       m = chars[c][k];
       if(k==7) hist[c] = m;
       if(!m)
       {
         if(k) hist[c] = chars[c][k-1]; // choose last one
         break;
       }
       if(m<=0x20) printf(" 0x%2.2X",m);
       else if(m<0x60) printf(" %c",m);
       else switch(m)
       {
         case 0x60: printf(" Ю"); break;
         case 0x61: printf(" А"); break;
         case 0x62: printf(" Б"); break;
         case 0x63: printf(" Ц"); break;
         case 0x64: printf(" Д"); break;
         case 0x65: printf(" Е"); break;
         case 0x66: printf(" Ф"); break;
         case 0x67: printf(" Г"); break;
         case 0x68: printf(" Х"); break;
         case 0x69: printf(" И"); break;
         case 0x6A: printf(" Й"); break;
         case 0x6B: printf(" К"); break;
         case 0x6C: printf(" Л"); break;
         case 0x6D: printf(" М"); break;
         case 0x6E: printf(" Н"); break;
         case 0x6F: printf(" О"); break;
         case 0x70: printf(" П"); break;
         case 0x71: printf(" Я"); break;
         case 0x72: printf(" Р"); break;
         case 0x73: printf(" С"); break;
         case 0x74: printf(" Т"); break;
         case 0x75: printf(" У"); break;
         case 0x76: printf(" Ж"); break;
         case 0x77: printf(" В"); break;
         case 0x78: printf(" Ь"); break;
         case 0x79: printf(" Ы"); break;
         case 0x7A: printf(" З"); break;
         case 0x7B: printf(" Ш"); break;
         case 0x7C: printf(" Э"); break;
         case 0x7D: printf(" Щ"); break;
         case 0x7E: printf(" Ч"); break;
       }
     }
     printf("   \t--> #%2.2X\n",hist[c]);
     c++;
   }
 }
 printf("\n");
 f = fopen(argv[1],"rb");
 if(f==NULL) return -2;
 png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING,NULL,NULL,NULL);
 if(!png) return -3;
 png_infop info = png_create_info_struct(png);
 if(!info) return -4;
 if(setjmp(png_jmpbuf(png))) return -5;
 png_init_io(png,f);
 png_read_info(png,info);
 x = png_get_image_width(png,info);
 y = png_get_image_height(png,info);
 if(png_get_color_type(png,info)!=PNG_COLOR_TYPE_GRAY)
 {
   printf("\nERROR: PNG image must be gray! %i\n\n",png_get_color_type(png,info));
   return -6;
 }
 png_bytep* img = (png_bytep*)malloc(sizeof(png_bytep)*y);
 for(j=0;j<y;j++) img[j] = (png_byte*)malloc(png_get_rowbytes(png,info));
 png_read_image(png,img);
 fclose(f);
 f = fopen("rk-pgraph.asm","wt");
 if(f==NULL) return -7;
 fb = fopen("rk-pgraph.bin","wb");
 if(fb==NULL){fclose(f);return -8;}
 png_destroy_read_struct(&png, &info, NULL);
 for(j=0;j<6;j++)
 {
   fprintf(f,"\tdb\t0,0,0,0,0,0,0,0,0,");
   for(i=0;i<64;i++) fprintf(f,"0,");
   fprintf(f,"0,0,0,0,0 ; %i\n",j);
   for(i=0;i<78;i++) fputc(0,fb);
 }
 e = 0;
 for(j=0;j<y;j++)
 {
   if(j>=50) break;
   fprintf(f,"\tdb\t0,0,0,0,0,0,0,0,0,");
   for(i=0;i<9;i++) fputc(0,fb);
   for(i=0;i<x;i++)
   {
      if(i>=128) break;
      c = img[j][i];
      printf("%c"," .:-=+*#%vnmVNM@"[c>>4]);
      e<<=1;e/=3; // the same as /1.5
#ifdef DEBUG
printf(" (%i,%i) brightness before %2.2X e=%i\n",i,j,c,e);
#endif
      c += e;
      e = 0;
      while(c<0){c++;e--;}
      while(c>255){c--;e++;}
#ifdef DEBUG
printf(" (%i,%i) brightness after %2.2X e=%i\n",i,j,c,e);
#endif
      if(i&1)
      { // second pixel in pair
        z2 = c/a;
#ifdef DEBUG
printf(" (%i,%i) z2 before %X\n",i,j,z2);
#endif
        if(z2>=3*h+1) z2=3*h;
        if(hist[(z1<<4)|z2]==0)
        {
          b = -1;
          k = 3*h+1;
          for(z=0;z<=3*h;z++)
          { // test every possibility
            if(hist[(z1<<4)|z])
            {
              if(z > z2)
              {
                if(z-z2<k){k=z-z2;b=z;}
              }
              else
              {
                if(z2-z<k){k=z2-z;b=z;}
              }
            }
          }
          if(b<0) return -9;
          z2 = b;
        }
        e2 = c-z2*a;
        e += e2;
#ifdef DEBUG
printf(" (%i,%i) z2 after %X e=%i --> %i\n",i,j,z2,e2,e);
#endif
        fprintf(f,"%i,",hist[(z1<<4)|z2]);
        fputc(hist[(z1<<4)|z2],fb);
#ifdef DEBUG
printf(" (%i,%i) >>>>>>>>>>>>>>>>>>>>>>> %2.2X\n",i,j,hist[(z1<<4)|z2]);
#endif
      }
      else
      { // first pixel in pair
        z1 = c/a;
#ifdef DEBUG
printf(" (%i,%i) z1 before %X\n",i,j,z1);
#endif
        if(z1>=3*h+1) z1=3*h;
        while(allz[z1]) z1++;
        e1 = c-z1*a;
        e += e1;
#ifdef DEBUG
printf(" (%i,%i) z1 after %X e=%i --> %i\n",i,j,z1,e1,e);
#endif
      }
   }
   printf("\n");
   fprintf(f,"0,0,0,0,0 ; %i\n",j+6);
   for(i=0;i<5;i++) fputc(0,fb);
 }
 while(j<55)
 {
   fprintf(f,"\tdb\t0,0,0,0,0,0,0,0,0,");
   for(i=0;i<64;i++) fprintf(f,"0,");
   fprintf(f,"0,0,0,0,0 ; %i\n",j+6);
   for(i=0;i<78;i++) fputc(0,fb);
   j++;
 }
 fclose(f);
 return 0;
}
