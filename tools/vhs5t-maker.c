/*
 vhs5t-maker.c - maker of VHS5T videos for RK86 by Shaos

 This code is PUBLIC DOMAIN! Use it on your own RISK!!!

 Build: gcc vhs5t-maker.c -lpng -o vhs5t-maker

 Usage: ./vhs5t-maker [raw-audio.r8u [frames-subfolder]]

 Must have vhs5t.rkr and rk-pgraph binary with sgr.bin in current directory

 Raw file for audio must be mono 8-bit unsigned 44100 Hz

 Frames must have 8-digit name and start with 00000001.png

 You also may have text file description with content for running title

 http://www.nedopc.org/forum/viewtopic.php?f=93&t=22417

 07-APR-2024 - started coding converter for "Bad Apple"
 14-APR-2024 - universal version publicly released
 15-APR-2024 - added optional description for title
*/

//#define BADAPPLE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#ifdef BADAPPLE
#include <png.h>
#endif

#define ATHR1 0x82
#define ATHR0 0x7E

//#define AUDIOTEST

#define FPS 7.5

#ifdef BADAPPLE
int savepng(char* filename, int width, int height, unsigned char* data)
{
   int bitdepth = 8;
   int colortype = PNG_COLOR_TYPE_GRAY;
   int transform = PNG_TRANSFORM_IDENTITY;
   int i = 0;
   int r = 0;
   FILE* fp = NULL;
   png_structp png_ptr = NULL;
   png_infop info_ptr = NULL;
   static png_bytep row_pointers[1024];
   if(NULL == data) { r = -1; goto endlabel; }
   if(!filename || !filename[0]) { r = -2; goto endlabel; }
   if(NULL == (fp = fopen(filename, "wb"))) { r = -4; goto endlabel; }
   if(NULL == (png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL))) { r = -5; goto endlabel; }
   if(NULL == (info_ptr = png_create_info_struct(png_ptr))) { r = -6; goto endlabel; }
   png_set_IHDR(png_ptr, info_ptr, width, height, bitdepth,
                colortype,          /* PNG_COLOR_TYPE_{GRAY, PALETTE, RGB, RGB_ALPHA, GRAY_ALPHA, RGBA, GA} */
                PNG_INTERLACE_NONE, /* PNG_INTERLACE_{NONE, ADAM7 } */
                PNG_COMPRESSION_TYPE_BASE,
                PNG_FILTER_TYPE_BASE);
   for(i = 0; i < height; ++i) row_pointers[i] = data + i*width;
   png_init_io(png_ptr, fp);
   png_set_rows(png_ptr, info_ptr, row_pointers);
   png_write_png(png_ptr, info_ptr, transform, NULL);
 endlabel:
   if(NULL != fp)
   {
     fclose(fp);
     fp = NULL;
   }
   if(NULL != png_ptr)
   {
     if(NULL == info_ptr) r = -7;
     png_destroy_write_struct(&png_ptr, &info_ptr);
   }
   return r;
}

#define PIX(v) printf("%c"," .:-=+*#%vnmVNM@"[v])

int tmp[480*7];
#endif

unsigned char img[128*50];
unsigned char *audio;
unsigned char player[768];
char description[1024] = "VIDEO ON RADIO-86RK (INTEL 8080 1.777 MHZ) 128X50@7.5FPS & 1-BIT SOUND 6KHZ...";

char prefix[16] = "frames128x50/";

int main(int argc, char** argv)
{
 FILE *f,*ff,*fo;
 char s[100],sa[64]="bad_apple.r8u";
 unsigned char uc;
 int x,y,i,j,k,kk,ii,iii,jj,jjj,c,cmin,cmax,sz,ai,ak,aa,pm,dlen;
 long tot=0;
 double d,pg,sum,nexti,step=30.0/FPS,astep=44100.0/803.2/FPS;
 if(argc>1) strcpy(sa,argv[1]);
 if(argc>2)
 {
    strcpy(prefix,argv[2]);
    if(prefix[strlen(prefix)-1]!='/')
       strcat(prefix,"/");
 }
 f = fopen("description","rb");
 if(f!=NULL)
 {
   fseek(f,0,SEEK_END);
   k = ftell(f);
   fseek(f,0,SEEK_SET);
   for(i=0;i<k;i++)
   {
     if(i>=1024) break;
     description[i] = toupper(fgetc(f));
   }
   if(i>=1024) description[1023] = 0;
   else description[i] = 0;
   fclose(f);
 }
 dlen = strlen(description);
 printf("DESC={%s}\n",description);
 for(i=0;i<768;i++) player[i] = 0;
 f = fopen(sa,"rb");
 if(f==NULL) return -1;
 fseek(f,0,SEEK_END);
 sz = ftell(f);
 fseek(f,0,SEEK_SET);
 pg = sz/44100.0*FPS/10;
 pm = (int)pg;
 printf("%3.2f pages (%i)\n",pg,pm);
 audio = (unsigned char*)malloc(sz);
 nexti = astep;
 sum = 0;
 ak = ai = 0;
 for(i=0;i<sz;i++)
 {
   ak++;
   sum += fgetc(f);
   if(i>nexti)
   {
      nexti += astep;
      c = sum/ak;
      if(c>255) c = 255;
      audio[ai++] = c;
      ak = 0;
      sum = 0;
   }
 }
 fclose(f);
 fo = fopen("romdisk.bin","wb");
 if(fo==NULL) return -2;
 ff = fopen("vhs5t.rkr","rb");
 if(ff==NULL){fclose(fo);return -3;}
 fseek(ff,0,SEEK_END);
 k = ftell(ff)-4;
 fseek(ff,4,SEEK_SET);
 for(i=0;i<768;i++)
 {
   if(i<k)
   {
      player[i] = fgetc(ff);
#if 1
      if(i>=0x10 && i<=0x3F) player[i] = description[i-0x10];
#endif
      fputc(player[i],fo);
   }
   else if(i==0x2BF) fputc('*',fo);
   else if(i>=0x280&&i<0x2BF) fputc('-',fo);
   else if(i==0x2C3||i==0x2C4||i==0x2C5) fputc('<',fo);
   else if(i>=0x2C7&&i<=0x2FF) fputc(description[0x2FF-i],fo);
   else fputc(0,fo);
 }
 fclose(ff);
 aa = ak = 0;
 nexti = 2.999;
 i = 1;
 while(1)
 {
#ifdef BADAPPLE
  if(i>nexti)
  {
   sprintf(s,"frames/%08d.tga",i);
   printf("open %s (%4.3f)\n",s,nexti);
   f = fopen(s,"rb");
   if(f==NULL) break;
   nexti += step;
   fseek(f,18,SEEK_SET);
   jjj = j = k = 0;
   cmin = 1000;
   cmax = 0;
   for(y=0;y<360;y++){
   for(x=0;x<480;x++){
     if(y>=5) tmp[j++]=fgetc(f)&31;
     else fgetc(f);
     fgetc(f);
     if(j>=480*7)
     {
       for(ii=0;ii<128;ii++)
       {
         d = ii*3.75;
         iii = (int)d;
         if(d-iii>=0.5) iii++;
         sum = 0.0;
         for(jj=0;jj<7;jj++)
           sum += tmp[480*jj+iii+1]+tmp[480*jj+iii+2]+tmp[480*jj+iii+3];
         sum /= 21.0;
         PIX((int)(sum/2));
         c = sum*8.22581; // 0..31 -> 0..255
         if(c>255) c = 255;
         if(c<cmin) cmin = c;
         if(c>cmax) cmax = c;
         img[jjj++] = c;
       }
       printf("\n");
       j = 0;
       k++;
     }
   }}
   tot++;
   fclose(f);
   printf("PNG %i (%i...%i)\n",savepng("128x50.png",128,50,img),cmin,cmax);
   sprintf(s,"cp 128x50.png %s%08d.png",prefix,tot);
   system(s);
   system("./rk-pgraph 128x50.png >out");
#else
   sprintf(s,"%s%08d.png",prefix,++tot);
   ff = fopen(s,"rb");
   if(ff==NULL) break;
   fclose(ff);
   sprintf(s,"./rk-pgraph %s%08d.png",prefix,tot);
   system(s);
#endif
   ff = fopen("rk-pgraph.bin","rb");
   if(ff!=NULL)
   {
     fseek(ff,0x1dd,SEEK_SET);
     for(jj=0;jj<50;jj++)
     {
       fread(img,1,78,ff);
       for(ii=63;ii>=0;ii--)
       {
         if((ii+1)&3) fputc(img[ii],fo);
         else
         {
            if(++ak>=ai) uc = 0;
            else uc = audio[ak];
            if(uc>ATHR1) aa = 128;
            if(uc<ATHR0) aa = 0;
#ifndef AUDIOTEST
            fputc(aa|img[ii],fo);
#else
            fputc(((ak&1)?128:0)|img[ii],fo);
#endif
         }
       }
     }
     fclose(ff);
   }
   if((tot%10)==0)
   {
     for(iii=767;iii>=0;iii--)
     {
       ii = 767-iii;
       jj = 6.3*tot/pm;
       kk = player[ii];
       if(ii==0x2BF-jj) kk = '*';
       else if(ii>=0x280&&ii<0x2C0) kk = '-';
       else if(ii==0x2C0) kk = (tot/10)%10+'0';
       else if(ii==0x2C1&&tot>=100) kk = (tot/100)%10+'0';
       else if(ii==0x2C2&&tot>=1000) kk = (tot/1000)%10+'0';
       else if(ii==0x2C3||ii==0x2C4||ii==0x2C5) kk = '<';
       else if(ii>=0x2C7&&ii<=0x2FF)
       {
          c = 0x2FF-ii+tot/10;
          while(c>=dlen) c-=dlen;
          kk = description[c];
       }
       if(ii>=0x280 && ((iii+1)&3)==0)
       {
          if(++ak>=ai) uc = 0;
          else uc = audio[ak];
          if(uc>ATHR1) aa = 128;
          if(uc<ATHR0) aa = 0;
#ifndef AUDIOTEST
          fputc(aa|kk,fo);
#else
          fputc(((ak&1)?128:0)|kk,fo);
#endif
       }
       else fputc(kk,fo);
     }
   }
#ifdef BADAPPLE
  }
  if(++i > 10000) break;
#endif
 }
 printf("Total %i frames\n",tot);
 printf("Audio %i/%i (%2.2f%%)\n",ak,ai,ak*100.0/ai);
 fclose(fo);
 return 0;
}
