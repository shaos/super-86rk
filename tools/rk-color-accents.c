#include <stdio.h>
#include <stdlib.h>
#include <string.h>

long cols[9]={0x0000FF,0x00FF00,0xFF0000,0xFFFFFF,0x0000AA,0x00AA00,0xAA0000,0x000000};

int main()
{
 char str[8],sta[8][8];
 int i,j,k,ii,jj;
 long l,ll[25];
 FILE *f;
 f = fopen("accents.htm","wt");
 if(f==NULL) return -1;
 ll[24] = 0xFFFFFF;
 fprintf(f,"<HTML><BODY BGCOLOR=#FFFFFF>\n");
 fprintf(f,"<h1>Colors of RK-tiles with accents</h1>\n");
 for(k=0;k<2;k++)
 {
   fprintf(f,"<h2>Palette %i</h2>\n",k);
   ii = 0;
   fprintf(f,"<table border=1 cellpadding=5>\n");
   for(j=0;j<9;j++)
   {
      strcpy(str,"000");
      fprintf(f,"<tr>\n");
      for(i=0;i<10;i++)
      {
         if(i==0)
         {
           if(j==0) fprintf(f," <td>&nbsp;");
           else if(j==1) fprintf(f," <td rowspan=4 align=center>Foreground<br>color<br>over black");
           else if(j==5) fprintf(f," <td rowspan=4 align=center>Special case<br>(all ones)");
         }
         else if(i==1)
         {
           if(j==0) fprintf(f," <td>Accent-->");
           else
           {
              fprintf(f," <td align=right>Attr %02d",ii++);
              if(ii==2) ii=10;
              if(ii==12) ii=0;
           }
         }
         else // i>=2
         {
           if(j==0)
           {
              fprintf(f," <td>%s",str);
              strcpy(sta[i-2],str);
              if(++str[2]=='2')
              {
                 str[2] = '0';
                 if(++str[1]=='2')
                 {
                    str[1] = '0';
                    str[0]++;
                 }
              }
           }
           else
           {
              jj = 25;
              l = cols[j-1];
              if(k)
              {
                if(l==0xFF0000) l=0x00FFFF;
                else if(l==0x00FF00) l=0xFF00FF;
                else if(l==0x0000FF) l=0xFFFF00;
                else if(l==0xAA0000) l=0x00AAAA;
                else if(l==0x00AA00) l=0xAA00AA;
                else if(l==0x0000AA) l=0xAAAA00;
                if(j<4) // subtract accents for palette 1
                {
                  if(sta[i-2][0]=='1'&&(l&0xFF0000)){l&=0x00FFFF;l|=0xAA0000;}
                  if(sta[i-2][1]=='1'&&(l&0x00FF00)){l&=0xFF00FF;l|=0x00AA00;}
                  if(sta[i-2][2]=='1'&&(l&0x0000FF)){l&=0xFFFF00;l|=0x0000AA;}
                }
                // compare colors with palette 0
                for(jj=0;jj<25;jj++) if(ll[jj]==l) break;
              }
              else if(j<4) // add accents for palette 0
              {
                if(sta[i-2][0]=='1'&&!(l&0xFF0000)) l|=0xAA0000;
                if(sta[i-2][1]=='1'&&!(l&0x00FF00)) l|=0x00AA00;
                if(sta[i-2][2]=='1'&&!(l&0x0000FF)) l|=0x0000AA;
                ll[((j-1)<<3)+i-2] = l; // store colors from paletter 0
              }
              if(jj<25) fprintf(f," <td bgcolor=#%6.6X align=right>*",l);
              else fprintf(f," <td bgcolor=#%6.6X>&nbsp;",l);
           }
         }
         fprintf(f,"</td>\n");
      }
      fprintf(f,"</tr>\n");
   }
   fprintf(f,"</table>\n");
 }
 fprintf(f,"<p>* Color is present in palette 0 as well.</b>\n");
 fprintf(f,"</BODY></HTML>\n");
 return 0;
}
