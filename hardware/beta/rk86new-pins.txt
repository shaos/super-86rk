Pinlist

Exported from rk86new.sch at 2/1/24 11:20 PM

EAGLE Version 5.12.0 Copyright (c) 1988-2011 CadSoft

Part     Pad      Pin        Dir      Net

C1       1        1          Pas      N$90
         2        2          Pas      GND

C2       1        1          Pas      N$45
         2        2          Pas      N$78

C3       +        +          Pas      N$29
         -        -          Pas      N$30

C4       1        1          Pas      N$29
         2        2          Pas      N$30

C5       1        1          Pas      N$60
         2        2          Pas      N$65

C6       1        1          Pas      GND
         2        2          Pas      N$65

C7       1        1          Pas      GND
         2        2          Pas      VCC

C8       1        1          Pas      GND
         2        2          Pas      VSS

C9       +        +          Pas      GND
         -        -          Pas      VSS

C10      +        +          Pas      GND
         -        -          Pas      VSS

C11      +        +          Pas      VDD
         -        -          Pas      GND

C12      +        +          Pas      VCC
         -        -          Pas      GND

C13      +        +          Pas      VCC
         -        -          Pas      GND

C14      +        +          Pas      VDD
         -        -          Pas      GND

C15      1        1          Pas      N$78
         2        2          Pas      GND

C16      1        1          Pas      N$81
         2        2          Pas      N$78

C17      +        +          Pas      VCC
         -        -          Pas      GND

C18      +        +          Pas      VDD
         -        -          Pas      GND

C20      +        +          Pas      VCC
         -        -          Pas      GND

C21      +        +          Pas      VCC
         -        -          Pas      GND

C22      +        +          Pas      VCC
         -        -          Pas      GND

C23      +        +          Pas      VCC
         -        -          Pas      GND

C24      1        1          Pas      VCC
         2        2          Pas      GND

C25      +        +          Pas      VCC
         -        -          Pas      GND

C26      +        +          Pas      VCC
         -        -          Pas      GND

C27      +        +          Pas      VCC
         -        -          Pas      GND

C28      +        +          Pas      VCC
         -        -          Pas      GND

C29      1        1          Pas      VCC
         2        2          Pas      GND

C30      1        1          Pas      VCC
         2        2          Pas      GND

C31      1        1          Pas      VCC
         2        2          Pas      GND

C32      1        1          Pas      VCC
         2        2          Pas      GND

C33      1        1          Pas      VCC
         2        2          Pas      GND

C34      1        1          Pas      VCC
         2        2          Pas      GND

C35      1        1          Pas      VCC
         2        2          Pas      GND

C36      1        1          Pas      VCC
         2        2          Pas      GND

C37      1        1          Pas      VCC
         2        2          Pas      GND

C38      1        1          Pas      VCC
         2        2          Pas      GND

C39      1        1          Pas      VCC
         2        2          Pas      GND

D1       A        A          Pas      N$90
         C        C          Pas      VCC

D3       A        A          Pas      GND
         C        C          Pas      N$61

IC1      1        RESET      Out      RESET
         2        RESIN      In       N$90
         3        RDYIN      In       N$23
         4        READY      Out      READY
         5        SYNC       In       N$26
         6        PHI2(TTL)  Out      CLK
         7        STSTB      Out      N$85
         8        GND        Pwr      GND
         9        VDD        Pwr      VDD
         10       PHI2       Out      N$3
         11       PHI1       Out      N$2
         12       OSC        Out      N$5
         13       TANK       In                *** unconnected ***
         14       XTAL2      In       N$20
         15       XTAL1      In       N$21
         16       VCC        Pwr      VCC

IC2      1        1          Pas      _IOR
         2        2          Pas      _WR
         3        3          Pas               *** unconnected ***
         4        4          Pas      _MEMW
         5        5          Pas               *** unconnected ***
         6        6          Pas      READY
         7        7          Pas      HLDA
         8        8          Pas      STROBE
         9        9          Pas      AEN
         10       10         Pas      HOLD
         11       11         Pas      _CS_DMA
         12       12         Pas      CLK
         13       13         Pas      RESET
         14       14         Pas      DACK
         15       15         Pas               *** unconnected ***
         16       16         Pas      GND
         17       17         Pas      DRQ
         18       18         Pas      GND
         19       19         Pas      GND
         20       20         Pas      GND
         21       21         Pas      D7
         22       22         Pas      D6
         23       23         Pas      D5
         24       24         Pas               *** unconnected ***
         25       25         Pas               *** unconnected ***
         26       26         Pas      D4
         27       27         Pas      D3
         28       28         Pas      D2
         29       29         Pas      D1
         30       30         Pas      D0
         31       31         Pas      VCC
         32       32         Pas      A0
         33       33         Pas      A1
         34       34         Pas      A2
         35       35         Pas      A3
         36       36         Pas               *** unconnected ***
         37       37         Pas      A4
         38       38         Pas      A5
         39       39         Pas      A6
         40       40         Pas      A7

IC3      1        CKB        In       N$4
         5        VCC        Pwr      VCC
         6        R0(1)      In       GND
         7        R0(2)      In       GND
         8        QD         Out      CCLK
         9        QC         Out      N$7
         10       GND        Pwr      GND
         11       QB         Out               *** unconnected ***
         12       QA         Out      N$4
         14       CKA        In       N$5

IC4      1        I0         In       _IOR
         2        I1         In       _WR
         3        O          Out      CRT_WR
         4        I0         In       N$7
         5        I1         In       CCLK
         6        O          Out      N$16
         7        GND        Pwr      GND
         8        O          Out      _CS_ROM
         9        I0         In       N$37
         10       I1         In       N$38
         11       O          Out      N$38
         12       I0         In       _CS_DMA
         13       I1         In       _CS_FXXX
         14       VCC        Pwr      VCC

IC5      1        I0         In       N$25
         2        I1         In       _MEMW
         3        O          Out      _RD
         4        I0         In       VRTC
         5        I1         In       HRTC
         6        O          Out      N$19
         7        GND        Pwr      GND
         8        O          Out      N$13
         9        I0         In       A11
         10       I1         In       N$15
         11       O          Out      N$14
         12       I0         In       A12
         13       I1         In       N$15
         14       VCC        Pwr      VCC

IC6      1        A10        Out      A10
         2        VSS/GND    Pwr      GND
         3        D4         I/O      D4
         4        D5         I/O      D5
         5        D6         I/O      D6
         6        D7         I/O      D7
         7        D3         I/O      D3
         8        D2         I/O      D2
         9        D1         I/O      D1
         10       D0         I/O      D0
         11       VBB/-5V    Pwr      VSS
         12       RESET      In       RESET
         13       HOLD       In       HOLD
         14       INT        In       GND
         15       PHI2       In       N$3
         16       INTE       Out      N$32
         17       DBIN       Out      N$25
         18       WR         Out      N$1
         19       SYNC       Out      N$26
         20       VCC/+5V    Pwr      VCC
         21       HLDA       Out      HLDA
         22       PHI1       In       N$2
         23       READY      In       READY
         24       WAIT       Out               *** unconnected ***
         25       A0         Out      A0
         26       A1         Out      A1
         27       A2         Out      A2
         28       VDD/+12V   Pwr      VDD
         29       A3         Out      A3
         30       A4         Out      A4
         31       A5         Out      A5
         32       A6         Out      A6
         33       A7         Out      A7
         34       A8         Out      A8
         35       A9         Out      A9
         36       A15        Out      A15
         37       A12        Out      A12
         38       A13        Out      A13
         39       A14        Out      A14
         40       A11        Out      A11

IC7      1        1          Pas      GND
         2        2          Pas      GND
         3        3          Pas      D0
         4        4          Pas      A8
         5        5          Pas      D1
         6        6          Pas      A9
         7        7          Pas      D2
         8        8          Pas      A10
         9        9          Pas      D3
         10       10         Pas      A11
         11       11         Pas      STROBE
         12       12         Pas      GND
         13       13         Pas      AEN
         14       14         Pas      VCC
         15       15         Pas      A12
         16       16         Pas      D4
         17       17         Pas      A13
         18       18         Pas      D5
         19       19         Pas      A14
         20       20         Pas      D6
         21       21         Pas      A15
         22       22         Pas      D7
         23       23         Pas               *** unconnected ***
         24       24         Pas      VCC

IC8      1        1          Pas      LC3
         2        2          Pas      LC2
         3        3          Pas      LC1
         4        4          Pas      LC0
         5        5          Pas      DRQ
         6        6          Pas      DACK
         7        7          Pas      HRTC
         8        8          Pas      VRTC
         9        9          Pas      _RD
         10       10         Pas      CRT_WR
         11       11         Pas      LPEN
         12       12         Pas      D0B
         13       13         Pas      D1B
         14       14         Pas      D2B
         15       15         Pas      D3B
         16       16         Pas      D4B
         17       17         Pas      D5B
         18       18         Pas      D6B
         19       19         Pas      D7B
         20       20         Pas      GND
         21       21         Pas      A0B
         22       22         Pas      _CS_CRT
         23       23         Pas      CC0
         24       24         Pas      CC1
         25       25         Pas      CC2
         26       26         Pas      CC3
         27       27         Pas      CC4
         28       28         Pas      CC5
         29       29         Pas      CC6
         30       30         Pas      CCLK
         31       31         Pas      IRQ
         32       32         Pas      HLGT
         33       33         Pas      GPA0
         34       34         Pas      GPA1
         35       35         Pas      VSP
         36       36         Pas      RVV
         37       37         Pas      LTEN
         38       38         Pas      LA1
         39       39         Pas      LA0
         40       40         Pas      VCC

IC9      1        I          In       N$32
         2        O          OC       RK_SND
         3        I          In       RESET
         4        O          OC       _RESET
         5        I          In       N$19
         6        O          OC       VID_SYNC
         7        GND        Pwr      GND
         8        O          OC       _A15
         9        I          In       A15
         10       O          OC       VIDEO
         11       I          In       IVID
         12       O          OC       N$83
         13       I          In       _ZERO_PAGE
         14       VCC        Pwr      VCC

IC10     1        I0         In       A15
         2        I1         In       N$25
         3        O          Out      N$34
         4        I0         In       _A15
         5        I1         In       N$37
         6        O          Out      _CS_RAM
         7        GND        Pwr      GND
         8        O          Out      _ZERO_PAGE
         9        I0         In       N$74
         10       I1         In       N$80
         11       O          Out      _A11
         12       I0         In       A11
         13       I1         In       A11
         14       VCC        Pwr      VCC

IC11     1        A          In       A12
         2        B          In       A13
         3        C          In       A14
         4        G2A        In       GND
         5        G2B        In       GND
         6        G1         In       A15
         7        Y7         Out      _CS_FXXX
         8        GND        Pwr      GND
         9        Y6         Out      _CS_DMA
         10       Y5         Out      _CS_DXXX
         11       Y4         Out      _CS_CXXX
         12       Y3         Out      _CS_MUS
         13       Y2         Out      _CS_EXT
         14       Y1         Out      _CS_ALT
         15       Y0         Out      _CS_KBD
         16       VCC        Pwr      VCC

IC12     1        A7         In       CC4
         2        A6         In       CC3
         3        A5         In       CC2
         4        A4         In       CC1
         5        A3         In       CC0
         6        A2         In       LC2
         7        A1         In       LC1
         8        A0         In       LC0
         9        O0         Hiz      N$8
         10       O1         Hiz      N$6
         11       O2         Hiz      N$9
         12       GND        Pwr      GND
         13       O3         Hiz      N$10
         14       O4         Hiz      N$11
         15       O5         Hiz      N$12
         16       O6         Hiz      CH6
         17       O7         Hiz      CH7
         18       !CE        In       VSP
         19       A10        In       CHAR2
         20       !OE        In       GND
         21       VPP        In       VCC
         22       A9         In       CC6
         23       A8         In       CC5
         24       VCC        Pwr      VCC

IC13     1        CLR        In       _RESET
         2        D          In       GND
         3        CLK        In       GND
         4        PRE        In       N$34
         5        Q          Out      N$37
         6        !Q         Out      N$15
         7        GND        Pwr      GND
         8        !Q         Out      N$18
         9        Q          Out               *** unconnected ***
         10       PRE        In       N$73
         11       CLK        In       N$16
         12       D          In       LTEN
         13       CLR        In       N$17
         14       VCC        Pwr      VCC

IC14     1        PA3        I/O      N$36
         2        PA2        I/O      N$35
         3        PA1        I/O      N$31
         4        PA0        I/O      N$22
         5        RD         In       _RD
         6        CS         In       _CS_KBD
         7        GND        Pwr      GND
         8        A1         In       A1B
         9        A0         In       A0B
         10       PC7        I/O      N$55
         11       PC6        I/O      N$56
         12       PC5        I/O      N$57
         13       PC4        I/O      N$66
         14       PC0        I/O      N$68
         15       PC1        I/O      N$86
         16       PC2        I/O      N$91
         17       PC3        I/O      N$58
         18       PB0        I/O      N$47
         19       PB1        I/O      N$48
         20       PB2        I/O      N$49
         21       PB3        I/O      N$50
         22       PB4        I/O      N$51
         23       PB5        I/O      N$52
         24       PB6        I/O      N$53
         25       PB7        I/O      N$54
         26       VCC        Pwr      VCC
         27       D7         I/O      D7B
         28       D6         I/O      D6B
         29       D5         I/O      D5B
         30       D4         I/O      D4B
         31       D3         I/O      D3B
         32       D2         I/O      D2B
         33       D1         I/O      D1B
         34       D0         I/O      D0B
         35       RESET      In       RESET
         36       WR         In       _WR
         37       PA7        I/O      N$44
         38       PA6        I/O      N$43
         39       PA5        I/O      N$40
         40       PA4        I/O      N$39

IC15     1        S0         In       N$33
         2        SR         In       N$33
         3        A          In       N$8
         4        QA         Out               *** unconnected ***
         5        B          In       N$6
         6        QB         Out               *** unconnected ***
         7        C          In       N$9
         8        QC         Out               *** unconnected ***
         9        D          In       N$10
         10       QD         Out               *** unconnected ***
         11       CLK        In       N$4
         12       GND        Pwr      GND
         13       CLR        In       N$18
         14       QE         Out               *** unconnected ***
         15       E          In       N$11
         16       QF         Out      IVID
         17       F          In       N$12
         18       QG         Out               *** unconnected ***
         19       G          In       N$33
         20       QH         Out      N$17
         21       H          In       N$33
         22       SL         In       N$33
         23       S1         In       N$16
         24       VCC        Pwr      VCC

IC17     1        VPP        In       VCC
         2        A12        In       N$14
         3        A7         In       A7
         4        A6         In       A6
         5        A5         In       A5
         6        A4         In       A4
         7        A3         In       A3
         8        A2         In       A2
         9        A1         In       A1
         10       A0         In       A0
         11       O0         Hiz      D0
         12       O1         Hiz      D1
         13       O2         Hiz      D2
         14       GND        Pwr      GND
         15       O3         Hiz      D3
         16       O4         Hiz      D4
         17       O5         Hiz      D5
         18       O6         Hiz      D6
         19       O7         Hiz      D7
         20       !CE        In       _CS_ROM
         21       A10        In       A10
         22       !OE        In       _RD
         23       A11        In       N$13
         24       A9         In       A9
         25       A8         In       A8
         27       !PGM       In       VCC
         28       VCC        Pwr      VCC

IC21     1        ON/CMP     Pas               *** unconnected ***
         2        -IN        In       N$65
         3        +IN        In       GND
         4        V-         Pwr      VSS
         5        ON         Pas               *** unconnected ***
         6        OUT        Out      N$24
         7        V+         Pwr      VCC
         8        CMP        Pas               *** unconnected ***

IC32     1        NC         NC                *** unconnected ***
         2        A16        In       N$42
         3        A14        In       A14
         4        A12        In       A12
         5        A7         In       A7
         6        A6         In       A6
         7        A5         In       A5
         8        A4         In       A4
         9        A3         In       A3
         10       A2         In       A2
         11       A1         In       A1
         12       A0         In       A0
         13       I/O0       I/O      D0
         14       I/O1       I/O      D1
         15       I/O2       I/O      D2
         16       GND        Pwr      GND
         17       I/O3       I/O      D3
         18       I/O4       I/O      D4
         19       I/O5       I/O      D5
         20       I/O6       I/O      D6
         21       I/O7       I/O      D7
         22       !CS1       In       _CS_RAM
         23       A10        In       A10
         24       !OE        In       _RD
         25       A11        In       A11
         26       A9         In       A9
         27       A8         In       A8
         28       A13        In       A13
         29       !WE        In       _WR
         30       CS2        In       _A15
         31       A15        In       N$41
         32       VCC        Pwr      VCC

IC33     1        CLR        In       _RESET
         2        D          In       D0
         3        CLK        In       _WR_FXXX
         4        PRE        In       ONE
         5        Q          Out      N$41
         6        !Q         Out      N$74
         7        GND        Pwr      GND
         8        !Q         Out      N$80
         9        Q          Out      N$42
         10       PRE        In       N$59
         11       CLK        In       _WR_FXXX
         12       D          In       D1
         13       CLR        In       _RESET
         14       VCC        Pwr      VCC

IC34     1        I0         In       N$1
         2        I1         In       GND
         3        O          Out      _WR
         4        I0         In       A1
         5        I1         In       GND
         6        O          Out      A1B
         7        GND        Pwr      GND
         8        O          Out      A0B
         9        I0         In       A0
         10       I1         In       GND
         11       O          Out      _WR_FXXX
         12       I0         In       _WR
         13       I1         In       _CS_FXXX
         14       VCC        Pwr      VCC

IC35     1        I0         In       _CS_CXXX
         2        I1         In       A11
         3        O          Out      N$84
         4        I0         In       _CS_CXXX
         5        I1         In       _A11
         6        O          Out      _CS_C800
         7        GND        Pwr      GND
         8        O          Out      _CS_D000
         9        I0         In       _CS_DXXX
         10       I1         In       A11
         11       O          Out      _CS_D800
         12       I0         In       _CS_DXXX
         13       I1         In       _A11
         14       VCC        Pwr      VCC

IC36     1        DIR        In       _RD
         2        A1         I/O      D0
         3        A2         I/O      D1
         4        A3         I/O      D2
         5        A4         I/O      D3
         6        A5         I/O      D4
         7        A6         I/O      D5
         8        A7         I/O      D6
         9        A8         I/O      D7
         10       GND        Pwr      GND
         11       B8         I/O      D7B
         12       B7         I/O      D6B
         13       B6         I/O      D5B
         14       B5         I/O      D4B
         15       B4         I/O      D3B
         16       B3         I/O      D2B
         17       B2         I/O      D1B
         18       B1         I/O      D0B
         19       G          In       N$89
         20       VCC        Pwr      VCC

IC37     1        I0         In       A15
         2        I1         In       _CS_ROM
         3        O          Out      N$89
         4        I0         In       N$83
         5        I1         In       N$61
         6        O          Out      N$72
         7        GND        Pwr      GND
         8        O          Out      N$82
         9        I0         In       _ZERO_PAGE
         10       I1         In       VRTC
         11       O          Out      N$66
         12       I0         In       N$72
         13       I1         In       N$82
         14       VCC        Pwr      VCC

IC38     1        I0         In       _ZERO_PAGE
         2        I1         In       N$84
         3        O          Out      _CS_CRT
         4        I0         In       N$83
         5        I1         In       N$84
         6        O          Out      _CS_C000
         7        GND        Pwr      GND
         8        O          Out      SYNCB
         9        I0         In       GND
         10       I1         In       N$26
         11       O          Out      N$88
         12       I0         In       _ZERO_PAGE
         13       I1         In       _ZERO_PAGE
         14       VCC        Pwr      VCC

IC39     1        D7         I/O      D7B
         2        D6         I/O      D6B
         3        D5         I/O      D5B
         4        D4         I/O      D4B
         5        D3         I/O      D3B
         6        D2         I/O      D2B
         7        D1         I/O      D1B
         8        D0         I/O      D0B
         9        CLK0       In       CLK
         10       OUT0       Out      N$75
         11       GATE0      In       _RESET
         12       GND        Pwr      GND
         13       OUT1       Out      N$76
         14       GATE1      In       _RESET
         15       CLK1       In       CLK
         16       GATE2      In       _RESET
         17       OUT2       Out      N$77
         18       CLK2       In       CLK
         19       A0         In       A0B
         20       A1         In       A1B
         21       CS         In       _CS_MUS
         22       RD         In       _RD
         23       WR         In       _WR
         24       VCC        Pwr      VCC

JP1      1        1          Pas      CHAR2
         2        2          Pas      N$86
         3        3          Pas      CHAR2
         4        4          Pas      GND

JP2      1        1          Pas      N$64
         2        2          Pas      N$86
         3        3          Pas      N$63
         4        4          Pas      N$91

LED1     A        A          Pas      VCC
         K        C          Pas      N$87

LED2     A        A          Pas      N$69
         K        C          Pas      VSS

LED3     A        A          Pas      VCC
         K        C          Pas      N$70

LED4     A        A          Pas      VDD
         K        C          Pas      N$71

LSP1     MP       MP         Pas      GND

LSP2     MP       MP         Pas      N$90

LSP3     MP       MP         Pas               *** unconnected ***

Q1       1        1          Pas      N$20
         2        2          Pas      N$21

R1       1        1          Pas      N$90
         2        2          Pas      VCC

R2       1        1          Pas      N$78
         2        2          Pas      N$64

R3       1        1          Pas      N$23
         2        2          Pas      VCC

R4       1        1          Pas      N$78
         2        2          Pas      N$63

R5       1        1          Pas      CHAR2
         2        2          Pas      VCC

R7       1        1          Pas      N$24
         2        2          Pas      N$61

R8       1        1          Pas      GND
         2        2          Pas      N$69

R9       1        1          Pas      GND
         2        2          Pas      N$70

R10      1        1          Pas      GND
         2        2          Pas      N$71

R11      1        1          Pas      N$75
         2        2          Pas      N$78

R12      1        1          Pas      N$76
         2        2          Pas      N$78

R13      1        1          Pas      N$77
         2        2          Pas      N$78

R14      1        1          Pas      N$78
         2        2          Pas      N$68

R15      1        1          Pas      VIDEO
         2        2          Pas      N$27

R16      1        1          Pas      N$27
         2        2          Pas      VID_SYNC

R17      1        1          Pas      N$27
         2        2          Pas      VCC

R18      1        1          Pas      N$28
         2        2          Pas      N$29

R19      1        1          Pas      GND
         2        2          Pas      N$29

R20      1        1          Pas      N$78
         2        2          Pas      RK_SND

R21      1        1          Pas      N$78
         2        2          Pas      GND

R22      1        1          Pas      N$45
         2        2          Pas      VCC

R23      1        1          Pas      GND
         2        2          Pas      N$60

R24      1        1          Pas      GND
         2        2          Pas      N$65

R25      1        1          Pas      N$88
         2        2          Pas      N$87

R26      1        1          Pas      N$73
         2        2          Pas      VCC

R27      1        1          Pas      VCC
         2        2          Pas      ONE

R28      1        1          Pas      VCC
         2        2          Pas      N$59

RN1      1        1          Pas      VCC
         2        2          Pas      N$8
         3        3          Pas      N$6
         4        4          Pas      N$9
         5        5          Pas      N$10
         6        6          Pas      N$11
         7        7          Pas      N$12
         8        8          Pas      N$33
         9        9          Pas      _IOR
         10       10         Pas      _MEMW

T1       1        C          Pas      VCC
         2        B          Pas      N$27
         3        E          Pas      N$28

T2       1        C          Pas      VCC
         2        B          Pas      N$45
         3        E          Pas      N$79

X1       1        1          Pas      VCC
         2        2          Pas      N$23
         3        3          Pas      N$85
         4        4          Pas      GND

X2       1        1          Pas      N$30
         2        2          Pas               *** unconnected ***
         2@1      3          Pas               *** unconnected ***
         2@2      4          Pas      GND

X3       1        1          Pas      D0B
         2        2          Pas      D1B
         3        3          Pas      D2B
         4        4          Pas      D3B
         5        5          Pas      D4B
         6        6          Pas      D5B
         7        7          Pas      D6B
         8        8          Pas      D7B
         9        9          Pas      _WR
         10       10         Pas      _RD
         11       11         Pas      A0B
         12       12         Pas      A1B
         13       13         Pas      _CS_ALT
         14       14         Pas      VCC
         15       15         Pas      GND

X4       1        1          Pas      D0B
         2        2          Pas      D1B
         3        3          Pas      D2B
         4        4          Pas      D3B
         5        5          Pas      D4B
         6        6          Pas      D5B
         7        7          Pas      D6B
         8        8          Pas      D7B
         9        9          Pas      _WR
         10       10         Pas      _RD
         11       11         Pas      A0B
         12       12         Pas      A1B
         13       13         Pas      _CS_EXT
         14       14         Pas      VCC
         15       15         Pas      GND

X5       1        1          Pas      N$60
         2        2          Pas               *** unconnected ***
         2@1      3          Pas               *** unconnected ***
         2@2      4          Pas      GND

X6       1        KL         Pas      GND
         2        KL         Pas      VSS
         3        KL         Pas      VCC
         4        KL         Pas      VDD

X7       1        1          Pas      N$81
         2        2          Pas               *** unconnected ***
         2@1      3          Pas               *** unconnected ***
         2@2      4          Pas      GND

X8       1        1          Pas      GND
         2        2          Pas      GND
         3        3          Pas      N$30
         4        4          Pas               *** unconnected ***

X9       1        1          Pas      IVID
         2        2          Pas      RVV
         3        3          Pas      HLGT
         4        4          Pas      GPA0
         5        5          Pas      GPA1
         6        6          Pas      CH6
         7        7          Pas      CH7
         8        8          Pas      LA0

X10      1        1          Pas      D0B
         2        2          Pas      D1B
         3        3          Pas      D2B
         4        4          Pas      D3B
         5        5          Pas      D4B
         6        6          Pas      D5B
         7        7          Pas      D6B
         8        8          Pas      D7B
         9        9          Pas      _WR
         10       10         Pas      _RD
         11       11         Pas      A0B
         12       12         Pas      A1B
         13       13         Pas      A2
         14       14         Pas      A3
         15       15         Pas      A4
         16       16         Pas      A5
         17       17         Pas      A6
         18       18         Pas      A7
         19       19         Pas      A8
         20       20         Pas      A9
         21       21         Pas      A10
         22       22         Pas      _CS_C000
         23       23         Pas      _CS_C800
         24       24         Pas      _CS_D000
         25       25         Pas      _CS_D800
         26       26         Pas      SYNCB
         27       27         Pas      VCC
         28       28         Pas      GND
         29       29         Pas      GND
         30       30         Pas      GND

X11      1        1          Pas      N$79
         2        2          Pas      GND

X12      1        1          Pas      N$22
         2        2          Pas      N$47
         3        3          Pas      N$31
         4        4          Pas      N$48
         5        5          Pas      N$35
         6        6          Pas      N$49
         7        7          Pas      N$36
         8        8          Pas      N$50
         9        9          Pas      N$39
         10       10         Pas      N$51
         11       11         Pas      N$40
         12       12         Pas      N$52
         13       13         Pas      N$43
         14       14         Pas      N$53
         15       15         Pas      N$44
         16       16         Pas      N$54

X13      1        1          Pas      N$68
         2        2          Pas      N$86
         3        3          Pas      N$91
         4        4          Pas      N$58
         5        5          Pas      N$66
         6        6          Pas      N$57
         7        7          Pas      N$56
         8        8          Pas      N$55
         9        9          Pas      VCC
         10       10         Pas      GND

X14      1        1          Pas      HRTC
         2        2          Pas      VRTC
         3        3          Pas      VCC
         4        4          Pas      GND

