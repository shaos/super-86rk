Netlist

Exported from rk86new.sch at 2/1/24 11:20 PM

EAGLE Version 5.12.0 Copyright (c) 1988-2011 CadSoft

Net      Part     Pad      Pin        Sheet

A0       IC17     10       A0         1
         IC2      32       32         1
         IC32     12       A0         1
         IC34     9        I0         1
         IC6      25       A0         1

A0B      IC14     9        A0         1
         IC34     8        O          1
         IC39     19       A0         1
         IC8      21       21         1
         X10      11       11         1
         X3       11       11         1
         X4       11       11         1

A1       IC17     9        A1         1
         IC2      33       33         1
         IC32     11       A1         1
         IC34     4        I0         1
         IC6      26       A1         1

A1B      IC14     8        A1         1
         IC34     6        O          1
         IC39     20       A1         1
         X10      12       12         1
         X3       12       12         1
         X4       12       12         1

A2       IC17     8        A2         1
         IC2      34       34         1
         IC32     10       A2         1
         IC6      27       A2         1
         X10      13       13         1

A3       IC17     7        A3         1
         IC2      35       35         1
         IC32     9        A3         1
         IC6      29       A3         1
         X10      14       14         1

A4       IC17     6        A4         1
         IC2      37       37         1
         IC32     8        A4         1
         IC6      30       A4         1
         X10      15       15         1

A5       IC17     5        A5         1
         IC2      38       38         1
         IC32     7        A5         1
         IC6      31       A5         1
         X10      16       16         1

A6       IC17     4        A6         1
         IC2      39       39         1
         IC32     6        A6         1
         IC6      32       A6         1
         X10      17       17         1

A7       IC17     3        A7         1
         IC2      40       40         1
         IC32     5        A7         1
         IC6      33       A7         1
         X10      18       18         1

A8       IC17     25       A8         1
         IC32     27       A8         1
         IC6      34       A8         1
         IC7      4        4          1
         X10      19       19         1

A9       IC17     24       A9         1
         IC32     26       A9         1
         IC6      35       A9         1
         IC7      6        6          1
         X10      20       20         1

A10      IC17     21       A10        1
         IC32     23       A10        1
         IC6      1        A10        1
         IC7      8        8          1
         X10      21       21         1

A11      IC10     12       I0         1
         IC10     13       I1         1
         IC32     25       A11        1
         IC35     10       I1         1
         IC35     2        I1         1
         IC5      9        I0         1
         IC6      40       A11        1
         IC7      10       10         1

A12      IC11     1        A          1
         IC32     4        A12        1
         IC5      12       I0         1
         IC6      37       A12        1
         IC7      15       15         1

A13      IC11     2        B          1
         IC32     28       A13        1
         IC6      38       A13        1
         IC7      17       17         1

A14      IC11     3        C          1
         IC32     3        A14        1
         IC6      39       A14        1
         IC7      19       19         1

A15      IC10     1        I0         1
         IC11     6        G1         1
         IC37     1        I0         1
         IC6      36       A15        1
         IC7      21       21         1
         IC9      9        I          1

AEN      IC2      9        9          1
         IC7      13       13         1

CC0      IC12     5        A3         1
         IC8      23       23         1

CC1      IC12     4        A4         1
         IC8      24       24         1

CC2      IC12     3        A5         1
         IC8      25       25         1

CC3      IC12     2        A6         1
         IC8      26       26         1

CC4      IC12     1        A7         1
         IC8      27       27         1

CC5      IC12     23       A8         1
         IC8      28       28         1

CC6      IC12     22       A9         1
         IC8      29       29         1

CCLK     IC3      8        QD         1
         IC4      5        I1         1
         IC8      30       30         1

CH6      IC12     16       O6         1
         X9       6        6          1

CH7      IC12     17       O7         1
         X9       7        7          1

CHAR2    IC12     19       A10        1
         JP1      1        1          1
         JP1      3        3          1
         R5       1        1          1

CLK      IC1      6        PHI2(TTL)  1
         IC2      12       12         1
         IC39     15       CLK1       1
         IC39     18       CLK2       1
         IC39     9        CLK0       1

CRT_WR   IC4      3        O          1
         IC8      10       10         1

D0       IC17     11       O0         1
         IC2      30       30         1
         IC32     13       I/O0       1
         IC33     2        D          1
         IC36     2        A1         1
         IC6      10       D0         1
         IC7      3        3          1

D0B      IC14     34       D0         1
         IC36     18       B1         1
         IC39     8        D0         1
         IC8      12       12         1
         X10      1        1          1
         X3       1        1          1
         X4       1        1          1

D1       IC17     12       O1         1
         IC2      29       29         1
         IC32     14       I/O1       1
         IC33     12       D          1
         IC36     3        A2         1
         IC6      9        D1         1
         IC7      5        5          1

D1B      IC14     33       D1         1
         IC36     17       B2         1
         IC39     7        D1         1
         IC8      13       13         1
         X10      2        2          1
         X3       2        2          1
         X4       2        2          1

D2       IC17     13       O2         1
         IC2      28       28         1
         IC32     15       I/O2       1
         IC36     4        A3         1
         IC6      8        D2         1
         IC7      7        7          1

D2B      IC14     32       D2         1
         IC36     16       B3         1
         IC39     6        D2         1
         IC8      14       14         1
         X10      3        3          1
         X3       3        3          1
         X4       3        3          1

D3       IC17     15       O3         1
         IC2      27       27         1
         IC32     17       I/O3       1
         IC36     5        A4         1
         IC6      7        D3         1
         IC7      9        9          1

D3B      IC14     31       D3         1
         IC36     15       B4         1
         IC39     5        D3         1
         IC8      15       15         1
         X10      4        4          1
         X3       4        4          1
         X4       4        4          1

D4       IC17     16       O4         1
         IC2      26       26         1
         IC32     18       I/O4       1
         IC36     6        A5         1
         IC6      3        D4         1
         IC7      16       16         1

D4B      IC14     30       D4         1
         IC36     14       B5         1
         IC39     4        D4         1
         IC8      16       16         1
         X10      5        5          1
         X3       5        5          1
         X4       5        5          1

D5       IC17     17       O5         1
         IC2      23       23         1
         IC32     19       I/O5       1
         IC36     7        A6         1
         IC6      4        D5         1
         IC7      18       18         1

D5B      IC14     29       D5         1
         IC36     13       B6         1
         IC39     3        D5         1
         IC8      17       17         1
         X10      6        6          1
         X3       6        6          1
         X4       6        6          1

D6       IC17     18       O6         1
         IC2      22       22         1
         IC32     20       I/O6       1
         IC36     8        A7         1
         IC6      5        D6         1
         IC7      20       20         1

D6B      IC14     28       D6         1
         IC36     12       B7         1
         IC39     2        D6         1
         IC8      18       18         1
         X10      7        7          1
         X3       7        7          1
         X4       7        7          1

D7       IC17     19       O7         1
         IC2      21       21         1
         IC32     21       I/O7       1
         IC36     9        A8         1
         IC6      6        D7         1
         IC7      22       22         1

D7B      IC14     27       D7         1
         IC36     11       B8         1
         IC39     1        D7         1
         IC8      19       19         1
         X10      8        8          1
         X3       8        8          1
         X4       8        8          1

DACK     IC2      14       14         1
         IC8      6        6          1

DRQ      IC2      17       17         1
         IC8      5        5          1

GND      C1       2        2          1
         C10      +        +          1
         C11      -        -          1
         C12      -        -          1
         C13      -        -          1
         C14      -        -          1
         C15      2        2          1
         C17      -        -          1
         C18      -        -          1
         C20      -        -          1
         C21      -        -          1
         C22      -        -          1
         C23      -        -          1
         C24      2        2          1
         C25      -        -          1
         C26      -        -          1
         C27      -        -          1
         C28      -        -          1
         C29      2        2          1
         C30      2        2          1
         C31      2        2          1
         C32      2        2          1
         C33      2        2          1
         C34      2        2          1
         C35      2        2          1
         C36      2        2          1
         C37      2        2          1
         C38      2        2          1
         C39      2        2          1
         C6       1        1          1
         C7       1        1          1
         C8       1        1          1
         C9       +        +          1
         D3       A        A          1
         IC1      8        GND        1
         IC10     7        GND        1
         IC11     4        G2A        1
         IC11     5        G2B        1
         IC11     8        GND        1
         IC12     12       GND        1
         IC12     20       !OE        1
         IC13     2        D          1
         IC13     3        CLK        1
         IC13     7        GND        1
         IC14     7        GND        1
         IC15     12       GND        1
         IC17     14       GND        1
         IC2      16       16         1
         IC2      18       18         1
         IC2      19       19         1
         IC2      20       20         1
         IC21     3        +IN        1
         IC3      10       GND        1
         IC3      6        R0(1)      1
         IC3      7        R0(2)      1
         IC32     16       GND        1
         IC33     7        GND        1
         IC34     10       I1         1
         IC34     2        I1         1
         IC34     5        I1         1
         IC34     7        GND        1
         IC35     7        GND        1
         IC36     10       GND        1
         IC37     7        GND        1
         IC38     7        GND        1
         IC38     9        I0         1
         IC39     12       GND        1
         IC4      7        GND        1
         IC5      7        GND        1
         IC6      14       INT        1
         IC6      2        VSS/GND    1
         IC7      1        1          1
         IC7      12       12         1
         IC7      2        2          1
         IC8      20       20         1
         IC9      7        GND        1
         JP1      4        4          1
         LSP1     MP       MP         1
         R10      1        1          1
         R19      1        1          1
         R21      2        2          1
         R23      1        1          1
         R24      1        1          1
         R8       1        1          1
         R9       1        1          1
         X1       4        4          1
         X10      28       28         1
         X10      29       29         1
         X10      30       30         1
         X11      2        2          1
         X13      10       10         1
         X14      4        4          1
         X2       2@2      4          1
         X3       15       15         1
         X4       15       15         1
         X5       2@2      4          1
         X6       1        KL         1
         X7       2@2      4          1
         X8       1        1          1
         X8       2        2          1

GPA0     IC8      33       33         1
         X9       4        4          1

GPA1     IC8      34       34         1
         X9       5        5          1

HLDA     IC2      7        7          1
         IC6      21       HLDA       1

HLGT     IC8      32       32         1
         X9       3        3          1

HOLD     IC2      10       10         1
         IC6      13       HOLD       1

HRTC     IC5      5        I1         1
         IC8      7        7          1
         X14      1        1          1

IRQ      IC8      31       31         1

IVID     IC15     16       QF         1
         IC9      11       I          1
         X9       1        1          1

LA0      IC8      39       39         1
         X9       8        8          1

LA1      IC8      38       38         1

LC0      IC12     8        A0         1
         IC8      4        4          1

LC1      IC12     7        A1         1
         IC8      3        3          1

LC2      IC12     6        A2         1
         IC8      2        2          1

LC3      IC8      1        1          1

LPEN     IC8      11       11         1

LTEN     IC13     12       D          1
         IC8      37       37         1

N$1      IC34     1        I0         1
         IC6      18       WR         1

N$2      IC1      11       PHI1       1
         IC6      22       PHI1       1

N$3      IC1      10       PHI2       1
         IC6      15       PHI2       1

N$4      IC15     11       CLK        1
         IC3      1        CKB        1
         IC3      12       QA         1

N$5      IC1      12       OSC        1
         IC3      14       CKA        1

N$6      IC12     10       O1         1
         IC15     5        B          1
         RN1      3        3          1

N$7      IC3      9        QC         1
         IC4      4        I0         1

N$8      IC12     9        O0         1
         IC15     3        A          1
         RN1      2        2          1

N$9      IC12     11       O2         1
         IC15     7        C          1
         RN1      4        4          1

N$10     IC12     13       O3         1
         IC15     9        D          1
         RN1      5        5          1

N$11     IC12     14       O4         1
         IC15     15       E          1
         RN1      6        6          1

N$12     IC12     15       O5         1
         IC15     17       F          1
         RN1      7        7          1

N$13     IC17     23       A11        1
         IC5      8        O          1

N$14     IC17     2        A12        1
         IC5      11       O          1

N$15     IC13     6        !Q         1
         IC5      10       I1         1
         IC5      13       I1         1

N$16     IC13     11       CLK        1
         IC15     23       S1         1
         IC4      6        O          1

N$17     IC13     13       CLR        1
         IC15     20       QH         1

N$18     IC13     8        !Q         1
         IC15     13       CLR        1

N$19     IC5      6        O          1
         IC9      5        I          1

N$20     IC1      14       XTAL2      1
         Q1       1        1          1

N$21     IC1      15       XTAL1      1
         Q1       2        2          1

N$22     IC14     4        PA0        1
         X12      1        1          1

N$23     IC1      3        RDYIN      1
         R3       1        1          1
         X1       2        2          1

N$24     IC21     6        OUT        1
         R7       1        1          1

N$25     IC10     2        I1         1
         IC5      1        I0         1
         IC6      17       DBIN       1

N$26     IC1      5        SYNC       1
         IC38     10       I1         1
         IC6      19       SYNC       1

N$27     R15      2        2          1
         R16      1        1          1
         R17      1        1          1
         T1       2        B          1

N$28     R18      1        1          1
         T1       3        E          1

N$29     C3       +        +          1
         C4       1        1          1
         R18      2        2          1
         R19      2        2          1

N$30     C3       -        -          1
         C4       2        2          1
         X2       1        1          1
         X8       3        3          1

N$31     IC14     3        PA1        1
         X12      3        3          1

N$32     IC6      16       INTE       1
         IC9      1        I          1

N$33     IC15     1        S0         1
         IC15     19       G          1
         IC15     2        SR         1
         IC15     21       H          1
         IC15     22       SL         1
         RN1      8        8          1

N$34     IC10     3        O          1
         IC13     4        PRE        1

N$35     IC14     2        PA2        1
         X12      5        5          1

N$36     IC14     1        PA3        1
         X12      7        7          1

N$37     IC10     5        I1         1
         IC13     5        Q          1
         IC4      9        I0         1

N$38     IC4      10       I1         1
         IC4      11       O          1

N$39     IC14     40       PA4        1
         X12      9        9          1

N$40     IC14     39       PA5        1
         X12      11       11         1

N$41     IC32     31       A15        1
         IC33     5        Q          1

N$42     IC32     2        A16        1
         IC33     9        Q          1

N$43     IC14     38       PA6        1
         X12      13       13         1

N$44     IC14     37       PA7        1
         X12      15       15         1

N$45     C2       1        1          1
         R22      1        1          1
         T2       2        B          1

N$47     IC14     18       PB0        1
         X12      2        2          1

N$48     IC14     19       PB1        1
         X12      4        4          1

N$49     IC14     20       PB2        1
         X12      6        6          1

N$50     IC14     21       PB3        1
         X12      8        8          1

N$51     IC14     22       PB4        1
         X12      10       10         1

N$52     IC14     23       PB5        1
         X12      12       12         1

N$53     IC14     24       PB6        1
         X12      14       14         1

N$54     IC14     25       PB7        1
         X12      16       16         1

N$55     IC14     10       PC7        1
         X13      8        8          1

N$56     IC14     11       PC6        1
         X13      7        7          1

N$57     IC14     12       PC5        1
         X13      6        6          1

N$58     IC14     17       PC3        1
         X13      4        4          1

N$59     IC33     10       PRE        1
         R28      2        2          1

N$60     C5       1        1          1
         R23      2        2          1
         X5       1        1          1

N$61     D3       C        C          1
         IC37     5        I1         1
         R7       2        2          1

N$63     JP2      3        3          1
         R4       2        2          1

N$64     JP2      1        1          1
         R2       2        2          1

N$65     C5       2        2          1
         C6       2        2          1
         IC21     2        -IN        1
         R24      2        2          1

N$66     IC14     13       PC4        1
         IC37     11       O          1
         X13      5        5          1

N$68     IC14     14       PC0        1
         R14      2        2          1
         X13      1        1          1

N$69     LED2     A        A          1
         R8       2        2          1

N$70     LED3     K        C          1
         R9       2        2          1

N$71     LED4     K        C          1
         R10      2        2          1

N$72     IC37     12       I0         1
         IC37     6        O          1

N$73     IC13     10       PRE        1
         R26      1        1          1

N$74     IC10     9        I0         1
         IC33     6        !Q         1

N$75     IC39     10       OUT0       1
         R11      1        1          1

N$76     IC39     13       OUT1       1
         R12      1        1          1

N$77     IC39     17       OUT2       1
         R13      1        1          1

N$78     C15      1        1          1
         C16      2        2          1
         C2       2        2          1
         R11      2        2          1
         R12      2        2          1
         R13      2        2          1
         R14      1        1          1
         R2       1        1          1
         R20      1        1          1
         R21      1        1          1
         R4       1        1          1

N$79     T2       3        E          1
         X11      1        1          1

N$80     IC10     10       I1         1
         IC33     8        !Q         1

N$81     C16      1        1          1
         X7       1        1          1

N$82     IC37     13       I1         1
         IC37     8        O          1

N$83     IC37     4        I0         1
         IC38     4        I0         1
         IC9      12       O          1

N$84     IC35     3        O          1
         IC38     2        I1         1
         IC38     5        I1         1

N$85     IC1      7        STSTB      1
         X1       3        3          1

N$86     IC14     15       PC1        1
         JP1      2        2          1
         JP2      2        2          1
         X13      2        2          1

N$87     LED1     K        C          1
         R25      2        2          1

N$88     IC38     11       O          1
         R25      1        1          1

N$89     IC36     19       G          1
         IC37     3        O          1

N$90     C1       1        1          1
         D1       A        A          1
         IC1      2        RESIN      1
         LSP2     MP       MP         1
         R1       1        1          1

N$91     IC14     16       PC2        1
         JP2      4        4          1
         X13      3        3          1

ONE      IC33     4        PRE        1
         R27      2        2          1

READY    IC1      4        READY      1
         IC2      6        6          1
         IC6      23       READY      1

RESET    IC1      1        RESET      1
         IC14     35       RESET      1
         IC2      13       13         1
         IC6      12       RESET      1
         IC9      3        I          1

RK_SND   IC9      2        O          1
         R20      2        2          1

RVV      IC8      36       36         1
         X9       2        2          1

STROBE   IC2      8        8          1
         IC7      11       11         1

SYNCB    IC38     8        O          1
         X10      26       26         1

VCC      C12      +        +          1
         C13      +        +          1
         C17      +        +          1
         C20      +        +          1
         C21      +        +          1
         C22      +        +          1
         C23      +        +          1
         C24      1        1          1
         C25      +        +          1
         C26      +        +          1
         C27      +        +          1
         C28      +        +          1
         C29      1        1          1
         C30      1        1          1
         C31      1        1          1
         C32      1        1          1
         C33      1        1          1
         C34      1        1          1
         C35      1        1          1
         C36      1        1          1
         C37      1        1          1
         C38      1        1          1
         C39      1        1          1
         C7       2        2          1
         D1       C        C          1
         IC1      16       VCC        1
         IC10     14       VCC        1
         IC11     16       VCC        1
         IC12     21       VPP        1
         IC12     24       VCC        1
         IC13     14       VCC        1
         IC14     26       VCC        1
         IC15     24       VCC        1
         IC17     1        VPP        1
         IC17     27       !PGM       1
         IC17     28       VCC        1
         IC2      31       31         1
         IC21     7        V+         1
         IC3      5        VCC        1
         IC32     32       VCC        1
         IC33     14       VCC        1
         IC34     14       VCC        1
         IC35     14       VCC        1
         IC36     20       VCC        1
         IC37     14       VCC        1
         IC38     14       VCC        1
         IC39     24       VCC        1
         IC4      14       VCC        1
         IC5      14       VCC        1
         IC6      20       VCC/+5V    1
         IC7      14       14         1
         IC7      24       24         1
         IC8      40       40         1
         IC9      14       VCC        1
         LED1     A        A          1
         LED3     A        A          1
         R1       2        2          1
         R17      2        2          1
         R22      2        2          1
         R26      2        2          1
         R27      1        1          1
         R28      1        1          1
         R3       2        2          1
         R5       2        2          1
         RN1      1        1          1
         T1       1        C          1
         T2       1        C          1
         X1       1        1          1
         X10      27       27         1
         X13      9        9          1
         X14      3        3          1
         X3       14       14         1
         X4       14       14         1
         X6       3        KL         1

VDD      C11      +        +          1
         C14      +        +          1
         C18      +        +          1
         IC1      9        VDD        1
         IC6      28       VDD/+12V   1
         LED4     A        A          1
         X6       4        KL         1

VIDEO    IC9      10       O          1
         R15      1        1          1

VID_SYNC IC9      6        O          1
         R16      2        2          1

VRTC     IC37     10       I1         1
         IC5      4        I0         1
         IC8      8        8          1
         X14      2        2          1

VSP      IC12     18       !CE        1
         IC8      35       35         1

VSS      C10      -        -          1
         C8       2        2          1
         C9       -        -          1
         IC21     4        V-         1
         IC6      11       VBB/-5V    1
         LED2     K        C          1
         X6       2        KL         1

_A11     IC10     11       O          1
         IC35     13       I1         1
         IC35     5        I1         1

_A15     IC10     4        I0         1
         IC32     30       CS2        1
         IC9      8        O          1

_CS_ALT  IC11     14       Y1         1
         X3       13       13         1

_CS_C000 IC38     6        O          1
         X10      22       22         1

_CS_C800 IC35     6        O          1
         X10      23       23         1

_CS_CRT  IC38     3        O          1
         IC8      22       22         1

_CS_CXXX IC11     11       Y4         1
         IC35     1        I0         1
         IC35     4        I0         1

_CS_D000 IC35     8        O          1
         X10      24       24         1

_CS_D800 IC35     11       O          1
         X10      25       25         1

_CS_DMA  IC11     9        Y6         1
         IC2      11       11         1
         IC4      12       I0         1

_CS_DXXX IC11     10       Y5         1
         IC35     12       I0         1
         IC35     9        I0         1

_CS_EXT  IC11     13       Y2         1
         X4       13       13         1

_CS_FXXX IC11     7        Y7         1
         IC34     13       I1         1
         IC4      13       I1         1

_CS_KBD  IC11     15       Y0         1
         IC14     6        CS         1

_CS_MUS  IC11     12       Y3         1
         IC39     21       CS         1

_CS_RAM  IC10     6        O          1
         IC32     22       !CS1       1

_CS_ROM  IC17     20       !CE        1
         IC37     2        I1         1
         IC4      8        O          1

_IOR     IC2      1        1          1
         IC4      1        I0         1
         RN1      9        9          1

_MEMW    IC2      4        4          1
         IC5      2        I1         1
         RN1      10       10         1

_RD      IC14     5        RD         1
         IC17     22       !OE        1
         IC32     24       !OE        1
         IC36     1        DIR        1
         IC39     22       RD         1
         IC5      3        O          1
         IC8      9        9          1
         X10      10       10         1
         X3       10       10         1
         X4       10       10         1

_RESET   IC13     1        CLR        1
         IC33     1        CLR        1
         IC33     13       CLR        1
         IC39     11       GATE0      1
         IC39     14       GATE1      1
         IC39     16       GATE2      1
         IC9      4        O          1

_WR      IC14     36       WR         1
         IC2      2        2          1
         IC32     29       !WE        1
         IC34     12       I0         1
         IC34     3        O          1
         IC39     23       WR         1
         IC4      2        I1         1
         X10      9        9          1
         X3       9        9          1
         X4       9        9          1

_WR_FXXX IC33     11       CLK        1
         IC33     3        CLK        1
         IC34     11       O          1

_ZERO_PAGE IC10     8        O          1
         IC37     9        I0         1
         IC38     1        I0         1
         IC38     12       I0         1
         IC38     13       I1         1
         IC9      13       I          1

