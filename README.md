# Super-86RK

Designing retro-computer on Intel 8080 processor that can work as one of the Soviet computers listed below:

- Radio-86RK (1986) - currently supported in Alpha/Beta
- Microsha (1987) - future
- Apogey-BK01 (1988) - future

with some new extensions as 1-chip SRAM memory instead of 8-chip DRAM etc.

Based on design of Radio-86RK-SRAM32K (version 2012): http://radio86rk.pbworks.com

Current prototypes Alpha and Beta have static memory extended to 128K
